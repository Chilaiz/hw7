a = {"b": 'b'}
a.clear()
print('a =', a)



d = {'b': 'b'}
x = d.copy()
print('d:',d)




ktoto = {'name': 'ilya', 'age': 26}
print('name: ', ktoto.get('name'))
print('age:', ktoto.get('age'))


price = {'lamba': 2500000}
print(price.items())


keys = {'name': 'ilya', 'age': 26, 'salary': 0}
print(keys.keys())

t = {'yabloko': 2}
print(t.values())


r = {'grusha': 2}
g = r.pop('grusha')
print('popped:', g)

ya = {'name': 'ilya', 'age': 22}
result = ya.popitem()
print('x = ', result)
print('ya:', ya)


# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
a = {"b": 'b'}
def clearr(a):
    a.clear()
    print(a)
clearr(a)


d = {'b': 'b'}
def copy():
    x = d.copy()
    print('x:',x)
copy()

# для интереса дальше попробовал вот так разницы по идее же нету?
def get():
    ktoto = {'name': 'ilya', 'age': 26}
    print('name: ', ktoto.get('name'))
    print('age:', ktoto.get('age'))
get()



def items():
    price = {'lamba': 2500000}
    print(price.items())
items()


def keys():
    keys = {'name': 'ilya', 'age': 26, 'salary': 0}
    print(keys.keys())
keys()



def values():
    t = {'yabloko': 2}
    print(t.values())
values()



def pop():
    r = {'grusha': 2}
    g = r.pop('grusha')
    print('popped:', g)
pop()



def popitems():
    ya = {'name': 'ilya', 'age': 22}
    result = ya.popitem()
    print('x = ', result)
    print('ya:', ya)
popitems()


# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()

h = {'one': 10, 'two': 20, 'three': None}
k = {'five': 30, 'two': 40, 'three': 50}
h.update(k)
print(h)

ktoto = {'name': 'ilya', 'age': 26}
age = ktoto.setdefault('age')
print(ktoto)
print(age)

# Напишите пример вложенной функции
def x():
    def y():
        print("y")
    y()
x()


# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую запись в файле
fileNames = ['file1', 'file2']
def write(files):
    for fileName in files:
        file = open(fileName, 'a')
        file.write(fileName)
        file.close()

write(fileNames)






# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую файл новыми записями
textMass = ['tx2','tx1']
def openAndWrite(arr):
    file = open('file1.txt', 'a')
    for text in arr:
        file.write(text)
    file.close()

openAndWrite(textMass)




# Напишите функцию считывающую данные из файла
def read(fileName):
    file = open(fileName, 'r')
    print(file.read())
    file.close()


read('file1.txt')

# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает только отфильтрованные данные
massUser = [{'name': "Jack", 'age': 10},{'name': "daniels", 'age': 20}]

def filterAndWrite(arr):
    file = open('file1.txt', 'a')
    for i in arr:
         if i['age'] >= 18:
             file.write(str(i))
    file.close()


filterAndWrite(massUser)
